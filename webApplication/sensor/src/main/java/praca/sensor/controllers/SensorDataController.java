package praca.sensor.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import praca.sensor.dto.SensorDataDto;
import praca.sensor.model.SensorData;
import praca.sensor.repository.SensorDataRepository;
import praca.sensor.service.SensorDataMapper;
import praca.sensor.service.SensorDataService;

import java.util.List;

@RestController
@RequestMapping("/api/sensor")

public class SensorDataController {
    @Autowired
    private SensorDataRepository sensorDataRepository;
    @Autowired
    private SensorDataService sensorDataService;


    @GetMapping
    private List<SensorData> getAllSensorData() {
        return sensorDataService.getAllSensorData();
    }

    @PostMapping
    private SensorDataDto postSensorData(@RequestBody SensorDataDto sensorDataDto) {

        return sensorDataService.createSensorData(sensorDataDto);
    }
}