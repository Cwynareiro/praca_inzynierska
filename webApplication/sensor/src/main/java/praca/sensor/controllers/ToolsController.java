package praca.sensor.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import praca.sensor.model.ToolsState;
import praca.sensor.repository.ToolsStateRepository;
import praca.sensor.service.ToolsStateService;

import java.util.List;

@RestController
@RequestMapping("/api/tools")
public class ToolsController {

    @Autowired
    private ToolsStateRepository toolsStateRepository;
    @Autowired
    private ToolsStateService toolsStateService;

    @GetMapping
    private List<ToolsState> getAllToolsStates() {
        return toolsStateService.getAllToolsState();
    }

    @PostMapping
    private ToolsState postToolsState (@RequestBody ToolsState toolsState){
        return toolsStateService.createToolsState(toolsState);
    }
    @PutMapping ("/{id}")
    private ToolsState updateToolState (@RequestBody ToolsState toolsState, @PathVariable String id){
        return toolsStateService.updateToolsState(toolsState,id);
    }
}
