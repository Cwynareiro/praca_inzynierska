package praca.sensor.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import praca.sensor.dto.SensorDataDto;
import praca.sensor.model.SensorData;
import praca.sensor.repository.SensorDataRepository;


import java.util.List;
import java.util.stream.Collectors;

@Service
public class SensorDataService {

    @Autowired
    private SensorDataRepository sensorDataRepository;
    @Autowired
    private SensorDataMapper sensorDataMapper;

    public List<SensorData> getAllSensorData(){
        List<SensorData> sensorData=sensorDataRepository.findAll();
        return sensorData;
    }
    public SensorDataDto createSensorData(SensorDataDto sensorDataDto){
        SensorData sensorData=sensorDataMapper.dtoToSensorData(sensorDataDto);
        java.util.Date date=new java.util.Date();
        sensorData.setDate(date);
        SensorData savedSensorData = sensorDataRepository.save(sensorData);
        return sensorDataMapper.sensorDataToDto(savedSensorData);
    }

    public List<SensorData> getTheNewest(){
        return sensorDataRepository.findTop1ByOrderByIdDesc()
                .stream()
                .collect(Collectors.toList());
    }
    public List<SensorData> getAllSensorDataOrdered(){
        return sensorDataRepository.findAllByOrderByIdDesc()
                .stream()
                .collect(Collectors.toList());
    }
}
