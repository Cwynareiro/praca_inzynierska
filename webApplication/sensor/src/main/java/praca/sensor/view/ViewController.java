package praca.sensor.view;


import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import praca.sensor.model.SensorData;
import praca.sensor.model.ToolsState;
import praca.sensor.service.SensorDataService;
import praca.sensor.service.ToolsStateService;
import praca.sensor.service.exceptions.ToolStateNotFound;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ViewController {
    private final SensorDataService sensorDataService;
    private final ToolsStateService toolsStateService;


    @GetMapping({"/", "","/index"})
    public ModelAndView home() {
        List<SensorData> newest = sensorDataService.getTheNewest();

        ModelAndView modelAndView = new ModelAndView("index");
        modelAndView.addObject("sensorData", newest);

        return modelAndView;
    }
    @GetMapping({"/fulldatabase", })
    public ModelAndView fulldatabase() {
        List<SensorData> allSensorData = sensorDataService.getAllSensorDataOrdered();

        ModelAndView modelAndView = new ModelAndView("fulldatabase");
        modelAndView.addObject("sensorData", allSensorData);

        return modelAndView;
    }

    @GetMapping("/editTools")
    public String editProfile( Model model) throws ToolStateNotFound {
        ToolsState toolsState = toolsStateService.getById("1");

        ToolsState updateToolsState = new ToolsState("1", toolsState.getFirstToolState(), toolsState.getSecondToolState());
        model.addAttribute("updateToolsState", updateToolsState);
        return "editTools";
    }

    @PostMapping("/editTools")
    public String editProfile(@Valid @ModelAttribute ToolsState toolsState, BindingResult bindingResult) throws ToolStateNotFound {
        if (bindingResult.hasErrors()) {
            return "editTools";
        }
        toolsStateService.updateToolsState(toolsState, "1");
        return "redirect:/editTools";
    }




}
