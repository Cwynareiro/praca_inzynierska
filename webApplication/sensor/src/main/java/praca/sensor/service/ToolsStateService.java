package praca.sensor.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import praca.sensor.model.ToolsState;
import praca.sensor.repository.ToolsStateRepository;
import praca.sensor.service.exceptions.ToolStateNotFound;

import java.util.List;

@Service
public class ToolsStateService {

    @Autowired
    private ToolsStateRepository toolsStateRepository;

    public List<ToolsState> getAllToolsState(){
        List<ToolsState> toolsStates=toolsStateRepository.findAll();
        return toolsStates;
    }
    public ToolsState createToolsState(ToolsState toolsState){

        ToolsState savedToolsState = toolsStateRepository.save(toolsState);
        return savedToolsState;
    }

    public ToolsState updateToolsState(ToolsState toolsState, String id){
        if(toolsStateRepository.existsById(id)){
            ToolsState toolsStateById = toolsStateRepository.findById(id).get();
            toolsStateById.setFirstToolState(toolsState.getFirstToolState());
            toolsStateById.setSecondToolState(toolsState.getSecondToolState());
            ToolsState savedToolState = toolsStateRepository.save(toolsStateById);
            return savedToolState;
        }
        else return toolsState;
    }

    public ToolsState getById(String id) throws ToolStateNotFound {
        return toolsStateRepository.findById(id)
                .orElseThrow(() -> new ToolStateNotFound());
    }
}
