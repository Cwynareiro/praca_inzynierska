package praca.sensor.repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import praca.sensor.model.SensorData;

import java.util.List;

@Repository
public interface SensorDataRepository extends CrudRepository<SensorData, String> {
    List<SensorData> findAll();
    List<SensorData> findAllByOrderByIdDesc();
    List<SensorData> findTop1ByOrderByIdDesc();


 }
