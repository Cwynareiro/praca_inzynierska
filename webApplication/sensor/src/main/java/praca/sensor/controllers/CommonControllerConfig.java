package praca.sensor.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import praca.sensor.service.exceptions.ToolStateNotFound;

@ControllerAdvice
public class CommonControllerConfig {

    @ExceptionHandler(ToolStateNotFound.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public void handleUserNotFound() {
    }
}
