#include <WiFiEsp.h>
#include <DHT.h> 
#include <Wire.h> 
#include <SoftwareSerial.h>

#define DHTPIN A0
#define DHTTYPE DHT22
DHT dht(DHTPIN, DHTTYPE);

SoftwareSerial Serial1(10, 11); // RX, TX

//dane lokalnej sieci
char ssid[] = "*******";
char password[] = "*******";


int status = WL_IDLE_STATUS; // the Wifi radio's status

//dane serwera aplikacji
char server[] = "********";

char firstToolState = '0';
char secondToolState = '0';

WiFiEspClient client;
int counter = 0;
int sendDataRatio=200;
void setup() {
    //zadanie 1
    Serial1.begin(9600);
    dht.begin();
    WiFi.init( & Serial1);
    
    //zadanie 2
    pinMode(5, OUTPUT);
    pinMode(6, OUTPUT);
    
    //zadanie3 
    while (status != WL_CONNECTED) {
        status = WiFi.begin(ssid, password);
    }
}

void loop() {
    //zadanie pobrania danych
    getToolsState();
    PowerTheTools();
    
    //zadanie wysłania danych 
    if (counter%sendDataRatio == 0) {
        float temperature = dht.readTemperature();
        float humidity = dht.readHumidity();
        postSensorData(temperature, humidity);
    }
    counter++;
}

void postSensorData(float temperature, float humidity) {
    if (client.connect(server, 8080)) {
        client.println("POST /api/sensor HTTP/1.1");
        client.println("Host: server:8080");
        client.println("Accept: */*");
        client.println("Content-Length: 54");
        client.println("Content-Type: application/json");
        client.println();
        client.print("{\"humidity\": \"");
        client.print(humidity);
        client.print("\",\"temperature\": \"");
        client.print(temperature);
        client.print("\"}");
    }
    client.stop();
}

void PowerTheTools() {
    if (firstToolState == '1') {
        digitalWrite(5, HIGH);
    } else {
        digitalWrite(5, LOW);
    }

    if (secondToolState == '1') {
        digitalWrite(6, HIGH);
    } else {
        digitalWrite(6, LOW);
    }
}
void getToolsState() {
    if (client.connect(server, 8080)) {
        int currentToolNumber = 1;
        client.println("GET /api/tools HTTP/1.1");
        client.println("Host: server:8080");
        client.println("Accept: */*");
        client.println("Content-Length: 54");
        client.println("Content-Type: application/json");
        client.println();
        while (client.available()) {
            char c = client.read();
            if (c == '_') {
                if (currentToolNumber == 1) {
                    firstToolState = client.read();
                    currentToolNumber++;
                } else {
                    secondToolState = client.read();
                    currentToolNumber++;
                }
            }
        }
        client.stop();
    }
}
