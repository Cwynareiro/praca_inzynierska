package praca.sensor.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import praca.sensor.model.ToolsState;

import java.util.List;


@Repository
public interface ToolsStateRepository extends CrudRepository<ToolsState, String> {
    List<ToolsState> findAll();

}
