package praca.sensor.service;

import org.springframework.stereotype.Component;
import praca.sensor.dto.SensorDataDto;
import praca.sensor.model.SensorData;

@Component
public class SensorDataMapper {

    public SensorDataDto sensorDataToDto (SensorData sensorData){
        return new SensorDataDto(sensorData.getTemperature(),sensorData.getHumidity());
    }
    public SensorData dtoToSensorData (SensorDataDto sensorDataDto){
        return new SensorData(null,null,sensorDataDto.getTemperature(),sensorDataDto.getHumidity());
    }
}
