## Table of contents
* [General info](#general-info)
* [Communication](#communication)
* [Technologies](#technologies)
* [Arduino module](#arduino-module)


## General info
This project is a web application created to cooperate with an Arduino module and it has two different tasks:
* Collecting and displaying data from a temperature and moisture sensor connected to the module. 
* Controlling the state of tools connected to the module. 

It was created as part of my engineer's thesis called "Using the concept of the Internet of Things for remote monitoring and controlling the microclimate in the greenhouse". 

## Communication 
The Arduino module communicates with application by sending HTTP requests to the applications API. Application has a different API for each task.

### Sensor task

```
/api/sensor
```

This API is used to upload and display sensor data. The data can be sent by using following JSON pattern:

```
{
	"temperature": "23.00",
	"humidity":"50.00"
}
```
### Tools task

```
/api/tools
```

This API is used to change and display tools state. The data can be changed by using following JSON pattern:

```
{
	"first_tool_state": "_1",
	"second_tool_state":"_0"
}
```
where "1" means on and "0" means off.
 
	
## Technologies
Project is created with:
* Spring framework
* mySQL
* JPA
* Maven

## Arduino module

The Arduino module was built using:

* Arduino UNO 
* DHT22 temperature and moisture sensor
* ESP8266-01S WiFi module for internet connectivity
* Two LED lights to simulate tools on/off state. 

The module is programmed to work in a following cycle: 

* Check tools state 
* Power/shut down the tools
* Send sensor data 

To avoid a large number of unnecessary data, while also maintaining the modules fast response time to tools state changes, the data is sent only every 200th cycle. This can be easily changed by modifying sendDataRatio variable.

